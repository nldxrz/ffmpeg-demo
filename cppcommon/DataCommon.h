//
//  DataCommon.h
//  PowerCam
//
//  Created by ws on 2021/6/3.
//

#ifndef DataCommon_h
#define DataCommon_h
typedef enum {
    AudioLayoutMono,
    AudioLayoutStero,
} AudioLayout;

typedef enum {
    AudioFormatS16,
    AudioFormatFlt,
} AudioFormat;
#endif /* DataCommon_h */

