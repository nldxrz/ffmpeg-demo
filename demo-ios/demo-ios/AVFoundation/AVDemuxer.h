//
//  AVDemuxer.h
//  demo-ios
//
//  Created by apple on 2020/10/3.
//  Copyright © 2020 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

/** 解封装的回调，用于接收解析结果
 *
 */
@protocol DemuxerProtocal<NSObject>

@end

/** 解析器对象；可用于解析音频文件(例如:.m4a)或者视频文件(例如 .mp4)
 *  基于AVFoundation封装，支持的解析文件格式如下：
 *  音频：m4a,
 */
@interface AVDemuxer : NSObject
@property(nonatomic,strong)AVAsset *asset;
@property(nonatomic,strong)AVAssetReader *assetReader;
@property(nonatomic,assign)BOOL autoDecode;

// 通过本地音视频资源初始化解析器
- (id)initWithURL:(NSURL*)localURL;

/** 实现解封装MP4文件，并且将其中的未压缩音视频数据读取出来
 */
- (void)startProcess;


// 对远程文件的解封装;貌似不支持对远程文件的读取
- (id)initWithRemoteURL:(NSURL*)remoteURL;
- (void)startRemoteProcess;

- (void)stopProcess;
@end
