//
//  MP3Extract.hpp
//  demo-ios
//
//  Created by zhuangsz on 2022/11/5.
//  Copyright © 2022 apple. All rights reserved.
//

#ifndef MP3Extract_hpp
#define MP3Extract_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <functional>

extern "C" {
#include "cppcommon/CLog.h"
#include <libavutil/avutil.h>
#include <libavutil/error.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersrc.h>
#include <libavfilter/buffersink.h>
#include <libavutil/opt.h>
#include <libavutil/channel_layout.h>
#include <libavutil/timestamp.h>
}

using namespace std;
class MP3Extract
{
public:
    typedef std::function<void (int)> CallbackFun;
    // sPath:包含音频的纯音频文件(例如wav,m4a等等)或者音视频文件(mp4、mov等等)
    MP3Extract(string sPath);
    ~MP3Extract();
    
    int64_t             bit_rate;
    int                 sample_rate;
    CallbackFun         callback;
    
    // 检查源文件文件是否可以支持转换为mp3格式
    bool invalid();
    
    // 从一个音视频文件中提取音频并转化成mp3编码的音频保存
    // dPath:保存mp3到构造函数参数path所在文件
    // return:返回是否成功，true则代表dPath成功生成mp3文件，否则没有
    // 备注：此方法为同步方法，会阻塞当前线程。
    bool extract(string dPath);
private:
    bool openSourceFile();
    bool openOutFile();
    bool add_audio_stream();
    bool initAudioFormatFilter(AVStream *in_stream);
    bool directExtract();
    int select_sample_rate(AVCodec *codec,int rate);
    int64_t select_channel_layout(AVCodec *codec,int64_t ch_layout);
    enum AVSampleFormat select_sample_format(AVCodec *codec,enum AVSampleFormat fmt);
    bool doDecodeAudio(AVPacket *packet);
    bool doEncodeAudio(AVFrame *frame);
    bool doWrite(AVPacket* packet,bool isVideo);
    void releaseSources();
    
    int audio_in_stream_index;
    int audio_ou_stream_index;
    
    AVFormatContext *inFmtCtx;
    AVFormatContext *ouFmtCtx;
    
    // 用于音频解码
    AVCodecContext  *audio_de_ctx;
    // 用于音频编码
    AVCodecContext  *audio_en_ctx;
    
    // 处理音频转换的滤镜管道
    AVFilterGraph *graph;
    // 输入滤镜上下文(滤镜实例)
    AVFilterContext *src_flt_ctx;
    // 输出滤镜上下文(滤镜实例)
    AVFilterContext *sink_flt_ctx;
    // 输入滤镜
    AVFilter        *src_flt;
    // 输出滤镜
    AVFilter        *sink_flt;

    bool            audio_need_convert;
    AVFrame         *audio_de_frame;
    AVFrame         *audio_en_frame;
    
    string          srcPath;
    string          dstPath;
    
    enum AVCodecID      src_id;
    enum AVSampleFormat src_fmt;
    int64_t             src_layout;
    int64_t             src_bit_rate;
    int                 src_sample_rate;
    
    enum AVCodecID      dst_id;
    enum AVSampleFormat dst_fmt;
    int64_t             dst_layout;
    int64_t             dst_bit_rate;
    int                 dst_sample_rate;

    int64_t            next_audio_pts;;
    
    AVFrame *get_audio_frame(enum AVSampleFormat smpfmt,int64_t ch_layout,int sample_rate,int nb_samples);
};

#endif /* MP3Extract_hpp */
