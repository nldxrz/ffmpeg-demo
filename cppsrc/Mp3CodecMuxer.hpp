//
//  Mp3Muxer.hpp
//  PowerCam
//
//  Created by ws on 2021/6/2.
//

#ifndef Mp3Muxer_hpp
#define Mp3Muxer_hpp

#include <stdio.h>
#include <string>
#include <iostream>
#include "DataCommon.h"

extern "C"{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswresample/swresample.h>
#include <libavutil/opt.h>
#include <libavutil/error.h>
}
using namespace std;

class Mp3CodecMuxer
{
public:
    Mp3CodecMuxer(string path);
    ~Mp3CodecMuxer();
    
    void write(uint8_t *data,int size,AudioFormat format,int nb_nbsamples,AudioLayout layout,int samplerate);
    
    void finishWrite();
    
    // 快捷方法，从一个音视频文件中提取音频并转化成mp3编码的音频保存
    // srcPath:包含音频的纯音频文件(例如wav,m4a等等)或者音视频文件(mp4、mov等等)
    // 保存mp3到构造函数参数path所在文件
    void extractAudio(string srcPath);
    
private:
    string mPath;
    bool  initCtx;
    int ptsIndex;
    AVFormatContext *pFormatCtx;
    AVCodecContext *pCodecCtx;
    SwrContext      *pSwrCtx;
    
    bool initMuxerCtx();
    void privateLeaseResources();
    void doEncode1(AVFormatContext*fmtCtx,AVCodecContext *cCtx,AVFrame *frame);
    
    static enum AVSampleFormat select_sample_fmt(const AVCodec *codec,enum AVSampleFormat sample_fmt);
    static int select_sample_rate(const AVCodec *codec,int defalt_sample_rate);
    static uint64_t select_channel_layout(const AVCodec *codec,uint64_t default_layout);
};
#endif /* Mp3Muxer_hpp */
