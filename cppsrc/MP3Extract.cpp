//
//  MP3Extract.cpp
//  demo-ios
//
//  Created by zhuangsz on 2022/11/5.
//  Copyright © 2022 apple. All rights reserved.
//

#include "MP3Extract.hpp"

MP3Extract::MP3Extract(string sPath):dstPath(""),srcPath(sPath),inFmtCtx(NULL),ouFmtCtx(NULL),
audio_in_stream_index(-1),
src_id(AV_CODEC_ID_NONE),src_fmt(AV_SAMPLE_FMT_NONE),src_layout(-1),src_sample_rate(-1),src_bit_rate(-1),
dst_id(AV_CODEC_ID_NONE),dst_fmt(AV_SAMPLE_FMT_NONE),dst_layout(-1),dst_sample_rate(-1),dst_bit_rate(-1),
audio_de_ctx(NULL),audio_en_ctx(NULL),
audio_de_frame(NULL),audio_en_frame(NULL),
next_audio_pts(0),audio_need_convert(false)
{
    bit_rate = 128*1000;
    sample_rate = 16000;
}

MP3Extract::~MP3Extract()
{
    
}

/** 检测是否有音频
 */
bool MP3Extract::invalid()
{
    if (!openSourceFile()) {
        LOGD("open source file failure");
        return false;
    }
    
    bool invalid = false;
    for (int i=0; i<inFmtCtx->nb_streams; i++) {
        AVStream *stream = inFmtCtx->streams[i];
        
        if (stream->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
            enum AVCodecID codeId = stream->codecpar->codec_id;
            if (avcodec_find_decoder(codeId) == NULL) {
                continue;
            }
            
            invalid = true;
            src_id = codeId;
            src_fmt = (AVSampleFormat)stream->codecpar->format;
            src_layout = stream->codecpar->channel_layout;
            src_sample_rate = stream->codecpar->sample_rate;
            src_bit_rate = stream->codecpar->bit_rate;
        }
    }
    
    return invalid;
}

/** 做转码(包括编码方式，码率，分辨率，采样率，采样格式等等的转换)
 */
bool MP3Extract::extract(string dPath)
{
    // 无法正常提取源文件格式
    if (!invalid()) {return false;}
    
    int ret = 0;
    dstPath = dPath;
    
    // 音频
    dst_id =  AV_CODEC_ID_MP3;
    dst_bit_rate = bit_rate;
    dst_sample_rate = sample_rate;    // 16khz
    dst_fmt = AV_SAMPLE_FMT_S16;
    dst_layout = AV_CH_LAYOUT_MONO;   // 单声道
    if (dst_id != src_id || dst_sample_rate != src_sample_rate || dst_fmt != src_fmt ||dst_layout != src_layout) {
        audio_need_convert = true;
    }
    
    // 不需要重新编解码，直接提取即可。
    if (!audio_need_convert) {
        LOGD("direct extract");
        if (callback != nullptr) {
            callback(100);
        }
        return directExtract();
    }
    if (callback != nullptr) {
        callback(0);
    }
    
    // 需要重新编解码
    // 打开输入文件及输出文件的上下文
    if (!openOutFile()) {
        LOGD("open out file failure!");
        return false;
    }
    
    // 为输出文件添加音频流
    if (audio_in_stream_index != -1) {
        if (!add_audio_stream()) {
            LOGD("add_audio_stream()");
            return false;
        }
    }
    
    // 打开解封装的上下文
    if (!(ouFmtCtx->oformat->flags & AVFMT_NOFILE)) {
        if (avio_open(&ouFmtCtx->pb, dstPath.c_str(), AVIO_FLAG_WRITE) < 0) {
            LOGD("avio_open fail");
            releaseSources();
            return false;
        }
    }
    
    
    // 写入头信息
    /** 遇到问题：avformat_write_header()崩溃
     *  分析原因：没有调用avio_open()函数
     *  解决方案：写成 !(ouFmtCtx->flags & AVFMT_NOFILE)即可
     */
    if ((ret = avformat_write_header(ouFmtCtx, NULL)) < 0) {
        LOGD("avformat_write_header fail %d",ret);
        releaseSources();
        return false;
    }
    
    if (!initAudioFormatFilter(inFmtCtx->streams[audio_in_stream_index])) {
        LOGD("init filter fail %d");
        releaseSources();
        return false;
    }
    
    // 读取源文件中的音视频数据进行解码
    AVPacket *inPacket = av_packet_alloc();
    while (av_read_frame(inFmtCtx, inPacket) == 0) {
        // 说明读取的音频数据
        if (inPacket->stream_index == audio_in_stream_index) {
            if (!doDecodeAudio(inPacket)) {
                return false;
            }
        }
        
        // 因为每一次读取的AVpacket的数据大小不一样，所以用完之后要释放
        av_packet_unref(inPacket);
    }
    LOGD("文件读取完毕");
    
    if (audio_in_stream_index != -1) {
        doDecodeAudio(NULL);
    }
    
    // 写入尾部信息
    av_write_trailer(ouFmtCtx);
    LOGD("成功结束");
    
    // 释放资源
    releaseSources();
    
    return true;
}

bool MP3Extract::openSourceFile()
{
    if (!srcPath.length()) {
        return false;
    }
    if (inFmtCtx != NULL) {
        return true;
    }
    
    int ret = 0;
    // 打开输入文件
    if ((ret = avformat_open_input(&inFmtCtx, srcPath.c_str(), NULL, NULL)) < 0) {
        LOGD("avformat_open_input() fail");
        releaseSources();
        return false;
    }
    if ((ret = avformat_find_stream_info(inFmtCtx, NULL)) < 0) {
        LOGD("avformat_find_stream_info fail %d",ret);
        releaseSources();
        return false;
    }
    
    for (int i=0; i<inFmtCtx->nb_streams; i++) {
        AVCodecParameters *codecpar = inFmtCtx->streams[i]->codecpar;
        if (codecpar->codec_type == AVMEDIA_TYPE_AUDIO && audio_in_stream_index == -1) {
            src_id = codecpar->codec_id;
            audio_in_stream_index = i;
        }
    }
    
    return true;
}

bool MP3Extract::openOutFile()
{
    if (!dstPath.length()) {
        return false;
    }
    if (ouFmtCtx != NULL) {
        return true;
    }
    int ret = 0;
    
    // 打开输出流
    if ((ret = avformat_alloc_output_context2(&ouFmtCtx, NULL, NULL, dstPath.c_str())) < 0) {
        LOGD("avformat_alloc_context fail %s",av_err2str(ret));
        releaseSources();
        return false;
    }
    
    return true;
}

/** 直接提取音频数据，并写入
 */
bool MP3Extract::directExtract()
{
    int ret = 0;
    if ((ret = avformat_alloc_output_context2(&ouFmtCtx,NULL,NULL,dstPath.c_str())) < 0) {
        LOGD("open out put file fail %s",av_err2str(ret));
        return false;
    }

    for (int i=0; i<inFmtCtx->nb_streams; i++) {
        AVStream *stream = inFmtCtx->streams[i];

        if (stream->codecpar->codec_type == AVMEDIA_TYPE_AUDIO && audio_in_stream_index == -1) {
            audio_in_stream_index = i;
            AVStream *newStream = avformat_new_stream(ouFmtCtx,NULL);
            audio_ou_stream_index = newStream->index;
            if ((ret = avcodec_parameters_copy(newStream->codecpar, stream->codecpar)) <0) {
                LOGD("parameters copy2 fail %s",av_err2str(ret));
                return false;
            }
            /** 遇到问题：avformat_write_header()函数提示"Tag mp4a incompatible with output codec id '86018'"
             *  分析原因：code_tag代表了音视频数据采用的码流格式。拿aac举例，AVI和MP4都支持aac编码的音频数据存储，MP4支持MKTAG('m', 'p', '4',
             *  'a')码流格式的aac流，avi支持(0x00ff,0x1600,0x706d等)，显然两者是不一样的，上面avcodec_parameters_copy()就相当于让封装和解封装的
             *  code_tag标签一模一样，所以造成了不一致的问题
             *  解决方案：将codecpar->codec_tag=0，系统会默认选择第一个匹配编码方式的codec_tag值
             */
            uint32_t src_codec_tag = stream->codecpar->codec_tag;
            if (av_codec_get_id(ouFmtCtx->oformat->codec_tag, src_codec_tag) != newStream->codecpar->codec_id) {
                newStream->codecpar->codec_tag = 0;
            }
        }
    }

    // 当flags没有AVFMT_NOFILE标记的时候才能调用avio_open2()函数进行初始化
    if (!(ouFmtCtx->flags & AVFMT_NOFILE)) {
        if ((ret = avio_open2(&ouFmtCtx->pb,dstPath.c_str(),AVIO_FLAG_WRITE,NULL,NULL)) < 0) {
            LOGD("io open fail %d",ret);
            return false;
        }
    }

    // 写入文件头信息
    if ((ret = avformat_write_header(ouFmtCtx, NULL)) < 0) {
        LOGD("write header fail %s",av_err2str(ret));
        return false;
    }

    AVPacket *in_packet = av_packet_alloc();
    while ((av_read_frame(inFmtCtx, in_packet)) == 0) {
        if (in_packet->stream_index != audio_in_stream_index) {
            continue;
        }
        LOGD("write packet index %d size %d",in_packet->stream_index,in_packet->size);
        AVStream *in_stream = inFmtCtx->streams[in_packet->stream_index];
        AVStream *ou_stream = NULL;
        if (in_stream->index == audio_in_stream_index) {
            ou_stream = ouFmtCtx->streams[audio_ou_stream_index];
        }

        /** 每个packet中pts,dts,duration 转换成浮点数时间的公式(以pts为例)：pts * timebase.num/timebase.den
         */
        in_packet->pts = av_rescale_q_rnd(in_packet->pts,in_stream->time_base,ou_stream->time_base,AV_ROUND_UP);
        in_packet->dts = av_rescale_q_rnd(in_packet->dts,in_stream->time_base,ou_stream->time_base,AV_ROUND_UP);
        in_packet->duration = av_rescale_q_rnd(in_packet->duration, in_stream->time_base, ou_stream->time_base, AV_ROUND_UP);
        in_packet->stream_index = ou_stream->index;
    
        av_write_frame(ouFmtCtx, in_packet);

        av_packet_unref(in_packet);
    }
    LOGD("over finish");
    av_write_trailer(ouFmtCtx);
    avformat_close_input(&inFmtCtx);
    avformat_free_context(ouFmtCtx);
    
    return true;
}

int MP3Extract::select_sample_rate(AVCodec *codec,int rate)
{
    int best_rate = 0;
    int deft_rate = 44100;
    bool surport = false;
    const int* p = codec->supported_samplerates;
    if (!p) {
        return deft_rate;
    }
    while (*p) {
        best_rate = *p;
        if (*p == rate) {
            surport = true;
            break;
        }
        p++;
    }
    
    if (best_rate != rate && best_rate != 0 && best_rate != deft_rate) {
        return deft_rate;
    }
    
    return best_rate;
}

enum AVSampleFormat MP3Extract::select_sample_format(AVCodec *codec,enum AVSampleFormat fmt)
{
    enum AVSampleFormat retfmt = AV_SAMPLE_FMT_NONE;
    enum AVSampleFormat deffmt = AV_SAMPLE_FMT_FLTP;
    const enum AVSampleFormat * fmts = codec->sample_fmts;
    if (!fmts) {
        return deffmt;
    }
    while (*fmts != AV_SAMPLE_FMT_NONE) {
        retfmt = *fmts;
        if (retfmt == fmt) {
            break;
        }
        fmts++;
    }
    
    if (retfmt != fmt && retfmt != AV_SAMPLE_FMT_NONE && retfmt != deffmt) {
        return deffmt;
    }
    
    return retfmt;
}

int64_t MP3Extract::select_channel_layout(AVCodec *codec,int64_t ch_layout)
{
    int64_t retch = 0;
    int64_t defch = AV_CH_LAYOUT_STEREO;
    const uint64_t * chs = codec->channel_layouts;
    if (!chs) {
        return defch;
    }
    while (*chs) {
        retch = *chs;
        if (retch == ch_layout) {
            break;
        }
        chs++;
    }
    
    if (retch != ch_layout && retch != AV_SAMPLE_FMT_NONE && retch != defch) {
        return defch;
    }
    
    return retch;
}

bool MP3Extract::add_audio_stream()
{
    if (ouFmtCtx == NULL) {
        LOGD("audio outformat NULL");
        releaseSources();
        return false;
    }
    
    // 添加一个音频流
    AVStream *stream = avformat_new_stream(ouFmtCtx, NULL);
    if (!stream) {
        LOGD("avformat_new_stream fail");
        return false;
    }
    audio_ou_stream_index = stream->index;
    
    AVCodecParameters *incodecpar = inFmtCtx->streams[audio_in_stream_index]->codecpar;
    AVCodec *codec = avcodec_find_encoder(dst_id);
    AVCodecContext *ctx = avcodec_alloc_context3(codec);
    if (ctx == NULL) {
        LOGD("audio codec ctx NULL");
        releaseSources();
        return false;
    }
    // 设置音频编码参数
    // 采样率
    int want_sample_rate = dst_sample_rate;
    int relt_sample_rate = select_sample_rate(codec, want_sample_rate);
    if (relt_sample_rate == 0) {
        LOGD("cannot surpot sample_rate");
        releaseSources();
        return false;
    }
    ctx->sample_rate = relt_sample_rate;
    // 采样格式
    enum AVSampleFormat want_sample_fmt = dst_fmt;
    enum AVSampleFormat relt_sample_fmt = select_sample_format(codec, want_sample_fmt);
    if (relt_sample_fmt == AV_SAMPLE_FMT_NONE) {
        LOGD("cannot surpot sample_fmt");
        releaseSources();
        return false;
    }
    ctx->sample_fmt  = relt_sample_fmt;
    // 声道格式
    int64_t want_ch = dst_layout;
    int64_t relt_ch = select_channel_layout(codec, want_ch);
    if (!relt_ch) {
        LOGD("cannot surpot channel_layout");
        releaseSources();
        return false;
    }
    ctx->channel_layout = relt_ch;
    // 声道数
    ctx->channels = av_get_channel_layout_nb_channels(ctx->channel_layout);
    // 编码后的码率
    ctx->bit_rate = dst_bit_rate;
    // frame_size 这里不用设置，调用avcodec_open2()函数后会根据编码类型自动设置
//    ctx->frame_size = incodecpar->frame_size;
    // 设置时间基
    ctx->time_base = (AVRational){1,ctx->sample_rate};
    stream->time_base = ctx->time_base;
    
    // 保存
    audio_en_ctx = ctx;
    
    // 设置编码的相关标记，这样进行封装的时候不会漏掉一些元信息
    if (ouFmtCtx->oformat->flags & AVFMT_GLOBALHEADER) {
        audio_en_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    }
    
    // 初始化编码器
    if (avcodec_open2(ctx, codec, NULL) < 0) {
        LOGD("audio avcodec_open2() fail");
        releaseSources();
        return false;
    }
    
    if (audio_en_ctx->frame_size != incodecpar->frame_size || relt_sample_fmt != incodecpar->format || relt_ch != incodecpar->channel_layout || relt_sample_rate != incodecpar->sample_rate) {
        audio_need_convert = true;
    }
    
    int ret = 0;
    // 将编码信息设置到音频流中
    if ((ret = avcodec_parameters_from_context(stream->codecpar, ctx)) < 0) {
        LOGD("audio copy audio stream fail");
        releaseSources();
        return false;
    }
    
    return true;
}

bool MP3Extract::initAudioFormatFilter(AVStream *in_stream)
{
    // 1、创建滤镜管道
    graph = avfilter_graph_alloc();
    
    // 2、创建源滤镜，用于接收要处理的AVFrame
    const AVFilter *src_filter = avfilter_get_by_name("abuffer");
    char src_args[200] = {0};
    sprintf(src_args, "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%" PRIX64,
            in_stream->time_base.num,in_stream->time_base.den,in_stream->codecpar->sample_rate,av_get_sample_fmt_name((enum AVSampleFormat)in_stream->codecpar->format),(uint64_t)in_stream->codecpar->channel_layout);
    int ret = avfilter_graph_create_filter(&src_flt_ctx, src_filter, NULL, src_args, NULL, graph);
    if (ret < 0) {
        LOGD("avfilter_graph_create_filter fail");
        releaseSources();
        return false;
    }
    
    // 3、创建输出滤镜，向外输出滤镜处理过的AVFrame
    const AVFilter *sink_filter = avfilter_get_by_name("abuffersink");
    if (!sink_filter) {
        LOGD("abuffersink not find");
        releaseSources();
        return false;
    }
    ret = avfilter_graph_create_filter(&sink_flt_ctx, sink_filter, NULL, NULL, NULL, graph);
    if (ret < 0) {
        LOGD("avfilter_graph_create_filter fail");
        releaseSources();
        return false;
    }
    /** 4、创建处理音频数据的各个滤镜(通过滤镜描述符)
     *  关于AVFilterInOut：
     *  1、它是一个链表，通过它可以用来将所有的滤镜串联起来，方便进行管理。开始于abuffer滤镜，结束于abuffersink滤镜;
     *  2、它就是为通过滤镜描述符创建滤镜来服务的，滤镜描述符按照指定的格式描述了一个个滤镜，而一个AVFilterInout代表了一个滤镜和滤镜上下文;
     *  4、需要单独释放，通过avfilter_inout_free()
     *
     *  tips：这里滤镜链描述符的语法格式和ffmpeg命令的滤镜语法格式一样
     */
    AVFilterInOut *inputs = avfilter_inout_alloc();
    AVFilterInOut *outputs = avfilter_inout_alloc();
    char filter_desc[200] = {0};
    
    /**     之前的写法为：sprintf(filter_desc, "aresample=%d,aformat=sample_fmts=%s:channel_layouts=%s",en_ctx->sample_rate,
     av_get_sample_fmt_name(en_ctx->sample_fmt),av_get_channel_name(en_ctx->channel_layout));

     *  遇到问题：部分声道类型通过av_get_channel_name(en_ctx->channel_layout)得到的声道类型名为null，所以导致创建滤镜时失败
     *  分析原因：暂时未知，日后看源码再备注
     *  解决方案：改成如下方式的滤镜描述符
     */
    sprintf(filter_desc, "aresample=%d,aformat=sample_fmts=%s:channel_layouts=0x%" PRIX64,audio_en_ctx->sample_rate,
            av_get_sample_fmt_name(audio_en_ctx->sample_fmt),audio_en_ctx->channel_layout);
    // 设置outputs的相关参数;outputs代表向滤镜描述符的第一个滤镜的输入端口输出数据，由于第一个滤镜的input label标签默认为"in"，
    // 所以这里outputs的name也设置为"in"
    outputs->name = av_strdup("in");
    outputs->filter_ctx = src_flt_ctx;
    outputs->pad_idx = 0;   // 对应滤镜上下文的端口索引号，单滤镜设为0即可
    outputs->next = NULL;   // 为NULL 代表单滤镜链
    
    // 设置inputs相关参数；inputs代表接受滤镜描述符的最后一个滤镜的输出端口输出的数据，由于最后一个滤镜的output label标签默认为"out"，
    // 所以这里inputs的name也设置为"out"
    inputs->name = av_strdup("out");
    inputs->filter_ctx = sink_flt_ctx;
    inputs->pad_idx = 0;    // 对应滤镜上下文的端口索引号，单滤镜设为0即可
    inputs->next = NULL;    // 为NULL 代表单滤镜链
    
    // 5、通过滤镜描述符创建并初始化各个滤镜并且按照滤镜描述符的顺序连接到一起
    if ((ret = avfilter_graph_parse_ptr(graph, filter_desc, &inputs, &outputs, NULL)) < 0) {
        LOGD("avfilter_graph_parse_ptr() fail %s",av_err2str(ret));
        releaseSources();
        return false;
    }
    // 设置abuffersink输出AVFrame的nbsamples与编码器需要的frame_size大小一致
    if (!(audio_en_ctx->codec->capabilities & AV_CODEC_CAP_VARIABLE_FRAME_SIZE)) {
        av_buffersink_set_frame_size(sink_flt_ctx, audio_en_ctx->frame_size);
    }
    
    // 6、配置滤镜管道(初始化滤镜)
    if (avfilter_graph_config(graph, NULL) < 0) {
        LOGD("avfilter_graph_config() fail");
        releaseSources();
        return false;
    }
    return true;
}

bool MP3Extract::doDecodeAudio(AVPacket *packet)
{
    int ret = 0;
    if (audio_de_ctx == NULL) {
        AVCodec *codec = avcodec_find_decoder(src_id);
        if (!codec) {
            LOGD("audio decoder not found");
            releaseSources();
            return false;
        }
        audio_de_ctx = avcodec_alloc_context3(codec);
        if (!audio_de_ctx) {
            LOGD("audio decodec_ctx fail");
            releaseSources();
            return false;
        }
        
        // 设置音频解码上下文参数;这里来自于源文件的拷贝
        if (avcodec_parameters_to_context(audio_de_ctx, inFmtCtx->streams[audio_in_stream_index]->codecpar) < 0) {
            LOGD("audio set decodec ctx fail");
            releaseSources();
            return false;
        }
        
        if ((avcodec_open2(audio_de_ctx, codec, NULL)) < 0) {
            LOGD("audio avcodec_open2 fail");
            releaseSources();
            return false;
        }
    }
    
    // 创建解码用的AVFrame
    if (!audio_de_frame) {
        audio_de_frame = av_frame_alloc();
    }
    
    if (!audio_en_frame) {
        audio_en_frame = av_frame_alloc();
        audio_en_frame->sample_rate = audio_en_ctx->sample_rate;
        audio_en_frame->channel_layout = audio_en_ctx->channel_layout;
        audio_en_frame->format = audio_en_ctx->sample_fmt;
        audio_en_frame->nb_samples = audio_en_ctx->frame_size;
        if(av_frame_get_buffer(audio_en_frame, 0) < 0) {
            LOGD("av_frame_get_buffer fail");
            releaseSources();
            return false;
        }
        if (av_frame_make_writable(audio_en_frame) < 0) {
            LOGD("av_frame_make_writable fail");
            releaseSources();
            return false;
        }
    }

    if ((ret = avcodec_send_packet(audio_de_ctx, packet)) < 0) {
        LOGD("audio avcodec_send_packet fail %d",ret);
        releaseSources();
        return false;
    }
    
    while (true) {
        if ((ret = avcodec_receive_frame(audio_de_ctx, audio_de_frame)) < 0) {
            if (ret == AVERROR_EOF) {
                LOGD("audio decode finish");
                // 解码缓冲区结束了，那么也要flush编码缓冲区
                doEncodeAudio(NULL);
            }
            break;
        }
        
        // 解码成功；利用音频滤镜进行格式转换
        ret = av_buffersrc_add_frame_flags(src_flt_ctx,audio_de_frame,AV_BUFFERSRC_FLAG_PUSH);
        if (ret < 0) {
            LOGD("av_buffersrc_add_frame() fail");
            releaseSources();
            return false;
        }
        
        while (true) {
            ret = av_buffersink_get_frame_flags(sink_flt_ctx, audio_en_frame,AV_BUFFERSINK_FLAG_NO_REQUEST);
            if (ret == AVERROR_EOF) {   // 说明结束了
                break;
            } else if (ret < 0) {
                break;
            }
            
            // 重新编码
            AVStream *stream = ouFmtCtx->streams[audio_ou_stream_index];
            audio_en_frame->pts = stream->time_base.den/stream->codecpar->sample_rate*next_audio_pts;
            next_audio_pts += audio_en_frame->nb_samples;
            doEncodeAudio(audio_en_frame);
        }
    }
    
    return true;
}

bool MP3Extract::doEncodeAudio(AVFrame *frame)
{
    int ret = 0;
    if ((ret = avcodec_send_frame(audio_en_ctx, frame)) < 0) {
        LOGD("audio avcodec_send_frame fail %s",av_err2str(ret));
        releaseSources();
        return false;
    }
    
    while (true) {
        AVPacket *pkt = av_packet_alloc();
        if ((ret = avcodec_receive_packet(audio_en_ctx, pkt)) < 0) {
            break;
        }
        
        // 说明编码得到了一个完整的帧
        // 因为调用avformat_write_header()函数后内部会改变ouFmtCtx->streams[audio_ou_stream_index]->time_base的值，而
        // pkt是按照audio_en_ctx->time_base来进行编码的，所以这里写入文件之前要进行时间的转换
        AVStream *stream = ouFmtCtx->streams[audio_ou_stream_index];
        av_packet_rescale_ts(pkt, audio_en_ctx->time_base, stream->time_base);
        pkt->stream_index = audio_ou_stream_index;
        
        doWrite(pkt,false);
    }
    
    return true;
}

bool MP3Extract::doWrite(AVPacket *packet,bool isVideo)
{
    if (!packet) {
        LOGD("packet is null");
        return false;
    }
    
    AVPacket *w_pkt = packet;
    AVStream *a_stream = ouFmtCtx->streams[audio_ou_stream_index];
    AVRational tb = a_stream->time_base;
    static int sum = 0;
    if (!isVideo) {
        sum++;
    }
    LOGD("%s pts %d(%s) dts %d du %d sum %d",!isVideo?"audio":"video",w_pkt->pts,av_ts2timestr(w_pkt->pts,&tb),w_pkt->dts,w_pkt->duration,sum);
    if ((av_write_frame(ouFmtCtx, w_pkt)) < 0) {
        LOGD("av_write_frame fail");
        return false;
    }
    
    av_packet_unref(w_pkt);
    
    return true;
}

AVFrame* MP3Extract::get_audio_frame(enum AVSampleFormat smpfmt,int64_t ch_layout,int sample_rate,int nb_samples)
{
    AVFrame * audio_en_frame = av_frame_alloc();
    // 根据采样格式，采样率，声道类型以及采样数分配一个AVFrame
    audio_en_frame->sample_rate = sample_rate;
    audio_en_frame->format = smpfmt;
    audio_en_frame->channel_layout = ch_layout;
    audio_en_frame->nb_samples = nb_samples;
    int ret = 0;
    if ((ret = av_frame_get_buffer(audio_en_frame, 0)) < 0) {
        LOGD("audio get frame buffer fail %d",ret);
        return NULL;
    }

    if ((ret =  av_frame_make_writable(audio_en_frame)) < 0) {
        LOGD("audio av_frame_make_writable fail %d",ret);
        return NULL;
    }
    
    return audio_en_frame;
}

void MP3Extract::releaseSources()
{
    if (inFmtCtx) {
        avformat_close_input(&inFmtCtx);
        inFmtCtx = NULL;
    }
    
    if (ouFmtCtx) {
        avformat_free_context(ouFmtCtx);
        ouFmtCtx = NULL;
    }
    
    if (audio_en_ctx) {
        avcodec_free_context(&audio_en_ctx);
        audio_en_ctx = NULL;
    }
    
    if (audio_de_ctx) {
        avcodec_free_context(&audio_de_ctx);
        audio_de_ctx = NULL;
    }
    
    if (audio_de_frame) {
        av_frame_free(&audio_de_frame);
    }
    
    if (audio_en_frame) {
        av_frame_free(&audio_en_frame);
        audio_en_frame = NULL;
    }
}
