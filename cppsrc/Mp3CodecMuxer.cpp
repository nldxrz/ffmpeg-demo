//
//  Mp3Muxer.cpp
//  PowerCam
//
//  Created by ws on 2021/6/2.
//

#include "Mp3CodecMuxer.hpp"
#include "CLog.h"

Mp3CodecMuxer::Mp3CodecMuxer(string path)
:mPath(path),pFormatCtx(NULL),pCodecCtx(NULL),initCtx(false),pSwrCtx(NULL),ptsIndex(0)
{
    
}

Mp3CodecMuxer::~Mp3CodecMuxer(){
    privateLeaseResources();
}

void Mp3CodecMuxer::privateLeaseResources()
{
    if (pFormatCtx) {
        avformat_free_context(pFormatCtx);
        pFormatCtx = NULL;
    }
    if (pCodecCtx) {
        avcodec_free_context(&pCodecCtx);
        pCodecCtx = NULL;
    }
    if (pSwrCtx) {
        swr_free(&pSwrCtx);
        pSwrCtx = NULL;
    }
}

void Mp3CodecMuxer::write(uint8_t *data,int size,AudioFormat format,int nb_nbsamples,AudioLayout layout,int samplerate)
{
    if (!initCtx) {
        initCtx = initMuxerCtx();
    }
    
    bool needConvert = false;
    AVFrame     *srcFrame = av_frame_alloc();
    AVFrame     *dstFrame = av_frame_alloc();
    // 每个声道音频的采样数大小
    srcFrame->nb_samples = nb_nbsamples;
    dstFrame->nb_samples = pCodecCtx->frame_size;
    // 采样格式
    srcFrame->format = format == AudioFormatS16 ? AV_SAMPLE_FMT_S16 : AV_SAMPLE_FMT_FLT;
    dstFrame->format = pCodecCtx->sample_fmt;
    // 声道格式
    srcFrame->channel_layout = layout == AudioLayoutMono ? AV_CH_LAYOUT_MONO : AV_CH_LAYOUT_STEREO;;
    dstFrame->channel_layout = pCodecCtx->channel_layout;
    // 采样率
    srcFrame->sample_rate = samplerate;
    dstFrame->sample_rate = pCodecCtx->sample_rate;
    // 通过以上参数即可确定一个AVFrame的大小了，即其中的音频数据的大小；
    // 然后通过此方法分配对应的内存块；第二个参数代表根据cpu的架构自动选择对齐位数，最好填写为0
    int ret = av_frame_get_buffer(srcFrame, 0);
    if (ret < 0) {
        LOGD("av_frame_get_buffer fail");
        privateLeaseResources();
        return;
    }
    // 让内存块可写，最好调用一下此方法
    ret = av_frame_make_writable(srcFrame);
    if (ret < 0) {
        LOGD("av_frame_make_writable fail");
        privateLeaseResources();
        return;
    }
    // 是否需要格式转换
    if (pCodecCtx->sample_fmt != srcFrame->format) {
        needConvert = true;
    }
    if (pCodecCtx->channel_layout != srcFrame->channel_layout) {
        needConvert = true;
    }
    if (pCodecCtx->sample_rate != srcFrame->sample_rate) {
        needConvert = true;
    }
    
    if (needConvert) {
        if (pSwrCtx == NULL) {
            pSwrCtx = swr_alloc_set_opts(NULL, pCodecCtx->channel_layout, pCodecCtx->sample_fmt, pCodecCtx->sample_rate,
                                         srcFrame->channel_layout, (enum AVSampleFormat)srcFrame->format, srcFrame->sample_rate, 0, NULL);
            if (pSwrCtx == NULL) {
                LOGD("swr_alloc_set_opts() fail");
                privateLeaseResources();
                return;
            }
        }
        
        ret = av_frame_get_buffer(dstFrame,0);
        if (ret < 0) {
            LOGD("av_frame_get_buffer fail %d",ret);
            privateLeaseResources();
            return;
        }
        ret = av_frame_make_writable(dstFrame);
        if (ret < 0) {
            LOGD("av_frame_make_writable %d",ret);
            privateLeaseResources();
            return;
        }
    }
    
    memset(srcFrame->data[0], 0, size);
    memcpy(srcFrame->data[0],data,size);
    free(data);
    data = NULL;
    
    AVStream *stream = pFormatCtx->streams[0];
    if (needConvert) {
        ret = swr_convert_frame(pSwrCtx,dstFrame,srcFrame);
        if (ret < 0) {
            LOGD("swr_convert_frame fail %d",ret);
            return;
        }
        dstFrame->pts = stream->time_base.den/stream->codecpar->sample_rate*ptsIndex;
        ptsIndex += dstFrame->nb_samples;
        doEncode1(pFormatCtx, pCodecCtx, dstFrame);
        // 释放资源
        av_frame_free(&srcFrame);
        av_frame_free(&dstFrame);
    } else {
        srcFrame->pts = stream->time_base.den/stream->codecpar->sample_rate*ptsIndex;
        ptsIndex += srcFrame->nb_samples;
        doEncode1(pFormatCtx,pCodecCtx,srcFrame);
        // 释放资源
        av_frame_free(&srcFrame);
    }
}

bool Mp3CodecMuxer::initMuxerCtx() {
    AVCodec *pCodec = avcodec_find_encoder_by_name("libmp3lame");
    if (pCodec == NULL) {
        LOGD("pCodec is null \n");
        return false;
    }
    
    pCodecCtx = avcodec_alloc_context3(pCodec);
    if (pCodecCtx == NULL) {
        LOGD("pCodecCtx is null \n");
        privateLeaseResources();
        return false;
    }
    
    pCodecCtx->bit_rate = 128000;
    pCodecCtx->sample_rate = select_sample_rate(pCodec,16000);
    pCodecCtx->sample_fmt = select_sample_fmt(pCodec, AV_SAMPLE_FMT_S16P);
    pCodecCtx->channel_layout = select_channel_layout(pCodec,AV_CH_LAYOUT_MONO);
    pCodecCtx->channels = av_get_channel_layout_nb_channels(pCodecCtx->channel_layout);
    
    int ret = avcodec_open2(pCodecCtx, pCodec, NULL);
    if (ret < 0) {
        LOGD("avcodec_open2 fail %d",ret);
        privateLeaseResources();
        return false;
    }
    
    if ((ret = avformat_alloc_output_context2(&pFormatCtx, NULL, NULL, mPath.c_str())) < 0) {
        LOGD("avformat_alloc_output_context2 fail %d",ret);
    }
    
    AVStream *ostream = avformat_new_stream(pFormatCtx, NULL);
    if (!ostream) {
        LOGD("avformat_new_stream fail");
        privateLeaseResources();
        return false;
    }
    
    ret = avio_open(&pFormatCtx->pb, mPath.c_str(), AVIO_FLAG_READ_WRITE);
    if (ret < 0) {
        LOGD("avio_open fail %d",ret);
        privateLeaseResources();
        return false;
    }
    
    avcodec_parameters_from_context(ostream->codecpar, pCodecCtx);
    ret = avformat_write_header(pFormatCtx, NULL);
    if (ret < 0) {
        LOGD("avformat_write_header failer");
        privateLeaseResources();
        return false;
    }
    
    return true;
}

void Mp3CodecMuxer::doEncode1(AVFormatContext*fmtCtx,AVCodecContext *cCtx,AVFrame *srcFrame)
{
    if (fmtCtx == NULL || cCtx == NULL) {
        return;
    }
    
    AVPacket  *packet = av_packet_alloc();
    int ret = 0;
    // 开始编码;对于音频编码来说，不需要设置pts的值(但是会出现警告);如果frame 为NULL，则代表将编码缓冲区中的所有剩余数据全部编码完
    ret = avcodec_send_frame(cCtx, srcFrame);
    while (ret >= 0) {
        ret = avcodec_receive_packet(cCtx, packet);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) { // EAGAIN 有可能是编码器需要先缓冲一部分数据，并不是真正的编码错误
//            if (ret == AVERROR_EOF) {
//                LOGD("encode error %d",ret);
//            }
            return;
        } else if (ret < 0) {   // 产生了真正的编码错误
            return;
        }
        av_write_frame(fmtCtx, packet);
        // 每次编码avcodec_receive_packet都会重新为packet分配内存，所以这里用完之后要主动释放
        av_packet_unref(packet);
    }
}

void Mp3CodecMuxer::finishWrite()
{
    doEncode1(pFormatCtx,pCodecCtx,NULL);
    int ret = av_write_trailer(pFormatCtx);
    if (ret != 0) {
        LOGD("封装失败");
    }
    privateLeaseResources();
}

void Mp3CodecMuxer::extractAudio(string srcPath) {
    
}

/**
 *  判断采样格式对于指定的编码器是否支持，如果支持则返回该采样格式；否则返回编码器支持的枚举值最大的采样格式
 */
enum AVSampleFormat Mp3CodecMuxer::select_sample_fmt(const AVCodec *codec,enum AVSampleFormat sample_fmt)
{
    const enum AVSampleFormat *p = codec->sample_fmts;
    enum AVSampleFormat rfmt = AV_SAMPLE_FMT_NONE;
    while (*p != AV_SAMPLE_FMT_NONE) {
        if (*p == sample_fmt) {
            return sample_fmt;
        }
        if (rfmt == AV_SAMPLE_FMT_NONE) {
            rfmt = *p;
        }
        p++;
    }
    
    return rfmt;
}

/**
 *  返回指定编码器接近尽量接近44100的采样率
 */
int Mp3CodecMuxer::select_sample_rate(const AVCodec *codec,int defalt_sample_rate)
{
    const int *p = 0;
    int best_samplerate = 0;
    if (!codec->supported_samplerates) {
        return 44100;
    }
    
    p = codec->supported_samplerates;
    while (*p) {
        if (*p == defalt_sample_rate) {
            return *p;
        }
        
        if (!best_samplerate || abs(44100 - *p) < abs(44100 - best_samplerate)) {
            best_samplerate = *p;
        }
        
        p++;
    }
    
    return best_samplerate;
}

/** 返回编码器支持的声道格式中声道数最多的声道格式
 *  声道格式和声道数一一对应
 */
uint64_t Mp3CodecMuxer::select_channel_layout(const AVCodec *codec,uint64_t default_layout)
{
    uint64_t best_ch_layout = AV_CH_LAYOUT_STEREO;
    const uint64_t *p = codec->channel_layouts;
    if (p == NULL) {
        return AV_CH_LAYOUT_STEREO;
    }
    int best_ch_layouts = 0;
    while (*p) {
        int layouts = av_get_channel_layout_nb_channels(*p);
        if (*p == default_layout) {
            return *p;
        }
        
        if (layouts > best_ch_layouts) {
            best_ch_layout = *p;
            best_ch_layouts = layouts;
        }
        p++;
    }
    
    
    return best_ch_layout;
}

